#!/bin/sh

#
# Perform a simple mysql query to verify that the mysql server is running
# The request verify that the specified database exist
# configuration is done with environment variables:
#   - MYSQL_USER
#   - MYSQL_PASSWORD
#   - MYSQL_DATABASE: the database name to check
#
# Dependencies:
# - mysql client
#
# Author: Gbouthenot
# Version: 1
# Revision: 1
#

mdb=performance_schema
muser=root

[ -n "$MYSQL_DATABASE" ] && mdb="$MYSQL_DATABASE"
[ -n "$MYSQL_USER" ] && muser="$MYSQL_USER"
[ -n "$MYSQL_ROOT_PASSWORD" ] && export MYSQL_PWD="$MYSQL_ROOT_PASSWORD"
[ -n "$MYSQL_PASSWORD" ] && export MYSQL_PWD="$MYSQL_PASSWORD"

res=$(mysql -u "${muser}" -BN -e "select count(*) from information_schema.schemata WHERE schema_name=\"${mdb}\"")
if [ "$res" -eq 1 2>/dev/null ] ; then
  exit 0
else
  echo "Healthcheck error: $res should be 1"
  exit 1
fi
