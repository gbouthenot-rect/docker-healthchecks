# What it is ?

This repository contains a list of healthchecks to be used with docker.

Each Healthcheck has a version number. This number must increase in case of a change that break backward compatibility.

For example, the file `healthcheck_memcached_v1.sh`  contains the v1 of the memcache healthcheck. If the test evolves to something that breaks compatibility, a new file `healthcheck_memcached_v2.sh` should be created, but the v1 file MUST still exist in the repository.

# How to use

Let's the main docker-compose.yml is in /apps/PROD/demo.
It has a `volumes` directory.

Clone the repository inside this directory:
```bash
mkdir -p volumes && cd volumes
git clone <repo url>
```

Then add volume, healthcheck and autoheal configuration in `docker-compose.yml`:
```yml
version: "2.4"
services:
  webssh:
    image: "docregistry.in.ac-besancon.fr:5000/acbesac/aphpssh:php7.4"
    restart: "unless-stopped"
    volumes:
      - "./volumes/docker-healthchecks/:/opt/docker-healthchecks/"
    environment:
      - "healthcheck_url=http://127.0.0.1/healthcheck_do_not_remove.txt"
      - "healthcheck_match=WEBSERVER_IS_OK"
      - "healthcheck_url=http://127.0.0.1/healthcheck_do_not_remove.php"
      - "healthcheck_matchh=PHP_IS_OK"
      - "healthcheck_proxy="
    healthcheck:
      test: "/opt/docker-healthchecks/healthcheck_curl_v1.sh"
      timeout: "15s"
      interval: "91s"
      retries: "3"
    labels:
      # set to true only when everything works
      - "autoheal=false"
      - "autoheal.stop.timeout=40"

  memcached:
    image: "memcached:1.6"
    restart: "unless-stopped"
    volumes:
      - "./volumes/docker-healthchecks/:/opt/docker-healthchecks/"
    healthcheck:
      test: "/opt/docker-healthchecks/healthcheck_memcached_v1.sh"
      interval: "1m30s"
      timeout: "10s"
      retries: "3"
      start_period: "40s"
      disable: "false"
    labels:
      # set to true only when everything works
      - "autoheal=false"
      - "autoheal.stop.timeout=10"

  mysql:
    image: "mariadb:10.6"
    restart: "unless-stopped"
    environment:
      - "MYSQL_USER=theuser"
      - "MYSQL_PASSWORD=thepassword"
      - "MYSQL_DATABASE=thedatabase"
    volumes:
      - "./volumes/db/:/var/lib/mysql/"
      - "./volumes/docker-healthchecks/:/opt/docker-healthchecks/"
    healthcheck:
      test: "/opt/docker-healthchecks/healthcheck_mysql_v1.sh"
      timeout: "15s"
      interval: "91s"
      retries: "3"
    labels:
      # set to true only when everything works
      - "autoheal=false"
      - "autoheal.stop.timeout=40"

  psql:
    image: "postgres:15.1"
    restart: "unless-stopped"
    environment:
      - "POSTGRES_PASSWORD=username"
      - "POSTGRES_USER=userpasswd"
     volumes:
      - "./volumes/db/:/var/lib/postgresql/data/"
      - "./volumes/docker-healthchecks/:/opt/docker-healthchecks/"
    healthcheck:
      test: "/opt/docker-healthchecks/healthcheck_psql_v1.sh"
      timeout: "15s"
      interval: "91s"
      retries: "3"
    labels:
      # set to true only when everything works
      - "autoheal=false"
      - "autoheal.stop.timeout=40"

  mongo:
    image: "mongo:4.0"
    restart: "unless-stopped"
    # https://docs.mongodb.com/manual/reference/parameters/
    command: "mongod --smallfiles --quiet --oplogSize 128 --setParameter internalQueryExecMaxBlockingSortBytes=67108864 --replSet rs0 --storageEngine=mmapv1"
    volumes:
      - "./volumes/configdb/:/data/configdb/"
      - "./volumes/db/:/data/db/"
      - "./volumes/docker-healthchecks/:/opt/docker-healthchecks/"
    environment:
      TZ: "Europe/Paris"
    healthcheck:
      test: "/opt/docker-healthchecks/healthcheck_mongo_v1.sh"
      interval: "1m30s"
      timeout: "10s"
      retries: "3"
      start_period: "40s"
    labels:
      # set to true only when everything works
      - "autoheal=false"
      - "autoheal.stop.timeout=30"
```

The autoheal part is optional, and it is advised to set it to *false* until the healthcheck works correctly. Then it can be set to *true*, and the docker-compose stack restarted.


# Healthcheck reference

Add a volume 
The cloned repository it typically mounted as a volume in the container : `/opt/docker-healthchecks/`
According to the [docker-compose specification](https://github.com/compose-spec/compose-spec/blob/master/spec.md):
> `healthcheck` declares a check that's run to determine whether or not containers for this service are "healthy". This overrides [HEALTHCHECK Dockerfile instruction](https://docs.docker.com/engine/reference/builder/#healthcheck) set by the service's Docker image.

# Healthcheck log
To see what is happening:
```
for cid in $(docker-compose ps -q) ; do docker inspect $cid | jq ".[] | { Name, Health: .State.Health }" ; done
```
