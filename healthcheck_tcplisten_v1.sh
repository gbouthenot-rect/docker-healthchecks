#!/bin/bash

#
# Perform a simple TCP check to verify that a given tcp port is open
#
# Dependencies:
# - bash
#
# Author: Gbouthenot
# Version: 1
# Revision: 1
#

# test tcp port
echo -n > /dev/tcp/$1/$2 || exit 1
