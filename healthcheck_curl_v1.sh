#!/bin/sh

#
# Perform a simple HTTP get request, and test for a match string
# configuration is done with environment variables:
#   - $healthcheck_url: the URL to test
#   - $healthcheck_match: the string to match (tested with grep -q)
#   - $healthcheck_proxy: the proxy to use
#
# Dependencies:
# - curl
#
# Author: Gbouthenot
# Version: 1
# Revision: 2
#

res=$(curl -sSN --proxy "${healthcheck_proxy}" "${healthcheck_url}") || exit $?

echo "$res" | grep -q "${healthcheck_match}"

if [ "$?" -eq 0 ] ; then
  exit 0
else
  echo "Healthcheck error: \"${healthcheck_url}\" cannot match \"${healthcheck_match}\". Got: \"${res}\""
  exit 1
fi
