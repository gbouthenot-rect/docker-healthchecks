#!/bin/bash

#
# Perform a simple TCP check to verify that the memcache server is running
#
# Dependencies:
# - bash
#
# Author: Gbouthenot
# Version: 1
# Revision: 1
#

cat </dev/null >/dev/tcp/127.0.0.1/11211
