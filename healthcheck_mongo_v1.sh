#!/bin/bash

# version history:
# v2: mongo6 compatible

# source: https://github.com/docker-library/healthcheck/blob/master/mongo/docker-healthcheck
set -eo pipefail

mongo=
command -v mongo &> /dev/null && mongo=mongo
command -v mongosh &> /dev/null && mongo=mongosh
[ -z "$mongo" ] && echo "Mongo not found" && exit 1

host="$(hostname --ip-address || echo '127.0.0.1')"

if "$mongo" --quiet "$host/test" --eval 'quit(db.runCommand({ ping: 1 }).ok ? 0 : 2)'; then
  exit 0
fi

exit 1
