#!/bin/sh

#
# Perform a simple mysql query to verify that the postgres server is running
#
# Author: Gbouthenot
# Version: 1
# Revision: 1
#

/usr/bin/pg_isready
